package pl.broadminds.cards.domain;

public enum OperationStatus {
    LOCK,
    SETTLED
}
