package pl.broadminds.cards.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors
@JsonIdentityInfo(generator= ObjectIdGenerators.IntSequenceGenerator.class, property="@cardOperationId")
public class CardOperation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

//    @NotNull
//    private String requestSourceId;
//
//    @NotNull
//    private String requestId;

    // constraint tylko na wartosci z enuma
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private CardOperationType type;

    @Basic
    @Column(nullable = false)
    private Instant createdTime;

    @Column(precision = 12, scale = 6, nullable = false)
    private BigDecimal amount;

    @Column(precision = 12, scale = 6, nullable = false)
    private BigDecimal balance;

//    @Basic
//    private Instant settlementTime;

    @ManyToOne
    @JoinColumn(name = "CARD_ID", nullable = false)
    private Card card;

    public static CardOperation of(Card card, BigDecimal amount, BigDecimal balance, CardOperationType debit) {
        return CardOperation.builder()
                .card(card)
                .amount(amount)
                .balance(balance)
                .type(debit)
                .createdTime(Instant.now())
                .build();
    }

}
