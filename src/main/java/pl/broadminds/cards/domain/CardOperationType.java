package pl.broadminds.cards.domain;

public enum CardOperationType {
    DEBIT, CREDIT;
}
