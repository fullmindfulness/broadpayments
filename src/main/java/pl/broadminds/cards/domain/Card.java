package pl.broadminds.cards.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import pl.broadminds.payments.domain.validation.ValidationMessages;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors
@JsonIdentityInfo(generator= ObjectIdGenerators.IntSequenceGenerator.class, property="@cardId")
@ApiModel(value = "Data of the card assigned to user's money account.",
        description = "Data of card assigned to user's money account.")
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, unique = true, length = 16)
    @ApiModelProperty(position = 0)
    private String number;

    @Column(nullable = false, unique = true, length = 11)
    @ApiModelProperty(hidden = true)
    private Integer csc;

    @Column(nullable = false)
    @ApiModelProperty(position = 1)
    private LocalDate expirationDate;

    @Column(precision = 12, scale = 6, nullable = false)
    @ApiModelProperty(position = 2)
    private BigDecimal balance;

    @Column(nullable = false, length = 3)
    @ApiModelProperty(position = 3)
    private Currency currency;

    @OneToMany(mappedBy = "card")
    @ApiModelProperty(position = 4)
    private List<CardOperation> operations;

//    @OneToMany(mappedBy = "card")
//    @Where(clause = "settlementTime IS NOT NULL")
//    @ApiModelProperty(readOnly = true)
//    private Set<CardOperation> settledOperations;
//
//    @OneToMany(mappedBy = "card")
//    @Where(clause = "settlementTime IS NULL")
//    @ApiModelProperty(readOnly = true)
//    private Set<CardOperation> unsettledOperations;

    public boolean hasEnoughFunds(BigDecimal amount) {
        return balance.compareTo(amount) >= 0;
//        return card.getBalance().subtract(card.getUnsettledOperations().stream()
//                .filter(operation -> operation.getType().equals(CardOperationType.DEBIT))
//                .map(CardOperation::getAmount)
//                .reduce(BigDecimal::add)
//                .orElse(BigDecimal.ZERO));
    }

    public Card debit(BigDecimal amount) {
        balance = balance.subtract(amount);
        return this;
    }

    public Card credit(BigDecimal amount) {
        balance = balance.add(amount);
        return this;
    }

}
