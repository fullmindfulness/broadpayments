package pl.broadminds.cards.business_exception;

public class InvalidCSCException extends CardException {

    public InvalidCSCException(String msg) {
        super(msg);
    }
}
