package pl.broadminds.cards.business_exception;

public class InsufficentFundsException extends CardException {

    public InsufficentFundsException(String msg) {
        super(msg);
    }
}
