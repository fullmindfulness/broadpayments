package pl.broadminds.cards.business_exception;

public class CardExpiredException extends CardException {

    public CardExpiredException(String msg) {
        super(msg);
    }
}
