package pl.broadminds.cards.business_exception;

import pl.broadminds.payments.business_exception.PayMeException;


public class CardException extends PayMeException {

    public CardException(String message) {
        super(message);
    }
}
