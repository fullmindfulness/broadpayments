package pl.broadminds.cards.business_exception;

public class CardNotFoundException extends CardException {

    public CardNotFoundException(String msg) {
        super(msg);
    }
}
