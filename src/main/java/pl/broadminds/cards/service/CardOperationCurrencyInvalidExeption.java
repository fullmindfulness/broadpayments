package pl.broadminds.cards.service;

import pl.broadminds.payments.business_exception.PayMeException;

public class CardOperationCurrencyInvalidExeption extends PayMeException {
    public CardOperationCurrencyInvalidExeption(String message) {
        super(message);
    }
}
