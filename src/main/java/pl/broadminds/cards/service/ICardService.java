package pl.broadminds.cards.service;

import org.javamoney.moneta.Money;
import pl.broadminds.cards.domain.Card;
import pl.broadminds.payments.business_exception.PayMeException;

import java.time.LocalDate;

public interface ICardService {

    Card findValidCard(String cardNumber, Integer csc, LocalDate expirationDate) throws PayMeException;

    Card debit(String cardNumber, Integer csc, Money money) throws PayMeException;

    Card credit(String cardNumber, Integer csc, Money money) throws PayMeException;

}
