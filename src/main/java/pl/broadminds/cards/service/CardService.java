package pl.broadminds.cards.service;

import org.javamoney.moneta.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.broadminds.cards.business_exception.CardExpiredException;
import pl.broadminds.cards.business_exception.CardNotFoundException;
import pl.broadminds.cards.domain.Card;
import pl.broadminds.cards.domain.CardOperation;
import pl.broadminds.cards.domain.CardOperationType;
import pl.broadminds.cards.repository.CardRepository;
import pl.broadminds.cards.repository.OperationRepository;
import pl.broadminds.payments.business_exception.PayMeException;
import pl.broadminds.payments.service.contract.ICurrencyConversionService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Currency;

@Service
public class CardService implements ICardService {

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private ICurrencyConversionService currencyConversionService;

    @Autowired
    private OperationRepository operationRepository;

    @Override
    public Card debit(String cardNumber, Integer csc, Money money) throws PayMeException {
        return debit(cardNumber, csc, money.getNumber().numberValue(BigDecimal.class), Currency.getInstance(money.getCurrency().getCurrencyCode()));
    }

    private Card debit(String cardNumber, Integer csc, BigDecimal amount, Currency currency) throws PayMeException {
        final Card card = findValidCard(cardNumber, csc);
        final BigDecimal convertedAmount = currencyConversionService.convertAsBigDecimal(amount, currency, card.getCurrency());
        if (!card.hasEnoughFunds(convertedAmount))
            throw new CardInsufficentEnoughFundsException(card.getBalance().setScale(4, RoundingMode.HALF_EVEN).toString());
        card.debit(convertedAmount);
        operationRepository.save(CardOperation.of(card, amount, card.getBalance(), CardOperationType.DEBIT));
        return cardRepository.save(card);
    }

    @Override
    public Card credit(String cardNumber, Integer csc, Money money) throws PayMeException {
        return credit(cardNumber, csc, money.getNumber().numberValue(BigDecimal.class), Currency.getInstance(money.getCurrency().getCurrencyCode()));
    }

    private Card credit(String cardNumber, Integer csc, BigDecimal amount, Currency currency) throws PayMeException {
        final Card card = findValidCard(cardNumber, csc);
        if (!card.getCurrency().equals(currency))
            throw new CardOperationCurrencyInvalidExeption(currency.getCurrencyCode());
        card.credit(amount);
        operationRepository.save(CardOperation.of(card, amount, card.getBalance(), CardOperationType.CREDIT));
        return card;
    }

    public Card findValidCard(String number, Integer csc) throws PayMeException {
        Card card = cardRepository.findByNumberAndCsc(number, csc)
                .orElseThrow(() -> new CardNotFoundException(number.toString()));
        if (LocalDate.now().isAfter(card.getExpirationDate()))
            throw new CardExpiredException(card.getNumber().toString());
        return card;
    }

    public Card findValidCard(String number, Integer csc, LocalDate expirationDate) throws PayMeException {
        Card card = cardRepository.findByNumberAndCscAndExpirationDate(number, csc, expirationDate)
                .orElseThrow(() -> new CardNotFoundException(number));
        if (LocalDate.now().isAfter(card.getExpirationDate()))
            throw new CardExpiredException(card.getNumber());
       return card;
    }
}
