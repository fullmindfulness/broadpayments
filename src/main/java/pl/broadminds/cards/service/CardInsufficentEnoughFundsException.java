package pl.broadminds.cards.service;

import pl.broadminds.cards.business_exception.CardException;

public class CardInsufficentEnoughFundsException extends CardException {
    public CardInsufficentEnoughFundsException(String message) {
        super(message);
    }
}
