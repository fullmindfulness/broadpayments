package pl.broadminds.cards.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.broadminds.cards.domain.Card;

import java.time.LocalDate;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "cards", path = "cards")
public interface CardRepository extends JpaRepository<Card, Long> {

    Optional<Card> findByNumber(String number);

    Optional<Card> findByNumberAndCsc(String number, Integer csc);

    Optional<Card> findByNumberAndCscAndExpirationDate(String number, Integer csc, LocalDate expirationDate);

//    Optional<Card> findByNumberAndCscAndExpirationDate(String number, String csc, LocalDate expirationDate);

//    Collection<Card> findAllByNumberIn(Collection<String> numbers);

}
