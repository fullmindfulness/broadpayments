package pl.broadminds.cards.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.broadminds.cards.domain.CardOperation;

@RepositoryRestResource(collectionResourceRel = "card-operations", path = "card-operations")
public interface OperationRepository extends JpaRepository<CardOperation, Long> {
}
