package pl.broadminds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PayMeAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(PayMeAppApplication.class, args);
	}
}
