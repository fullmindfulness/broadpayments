package pl.broadminds.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
//import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@Configuration
//@EnableSwagger2WebMvc
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
//@Import(SpringDataRestConfiguration.class)
//@Import()
public class SetupSwagger {

    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.any())
//                .paths(PathSelectors.any())



                .paths(PathSelectors.regex("/public/*"))
                .paths(PathSelectors.regex("/api/user/*"))
                .paths(PathSelectors.regex("/api/admin/*"))


//                .paths(PathSelectors.regex("/error"))
//

//                .paths(PathSelectors.regex("/api/profile").negate())
//                .paths(PathSelectors.regex("/api/cards.*").negate())
//                .paths(PathSelectors.regex("/api/card-operations.*").negate())
//                .paths(PathSelectors.regex("/api/account-operations.*").negate())
//                .paths(PathSelectors.regex("/api/system-parameters.*").negate())
//                .paths(PathSelectors.regex("/api/account.*/card.*").negate())
                .build();
//                .ignoredParameterTypes(Account.class)
//                .ignoredParameterTypes(AccountOperation.class)
//                .ignoredParameterTypes(CardOperation.class);
    }

}
