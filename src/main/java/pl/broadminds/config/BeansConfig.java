package pl.broadminds.config;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.time.Instant;
import java.util.Properties;
import java.util.TimeZone;

@Configuration
public class BeansConfig {


    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();

        SimpleModule module = new SimpleModule();
        module.addSerializer(new MoneySerializer());
        objectMapper.registerModule(module);

        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.configOverride(Instant.class).setFormat(
                JsonFormat.Value.forPattern("yyyy-MM-dd HH:mm:ss.SSS").withTimeZone(TimeZone.getDefault()));

        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        return objectMapper;
    }

    @Bean
    public JavaMailSender getMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername("tester.oprogramowania.pro@gmail.com");
        mailSender.setPassword("a1H78jn$%");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }

    @Bean
    public RepositoryRestConfigurer repositoryRestConfigurer() {

        return new RepositoryRestConfigurer() {

            @Override
            public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
//                config.setDefaultMediaType(MediaType.APPLICATION_JSON);
//                config.useHalAsDefaultJsonMediaType(false);
//                "application/hal+json"
            }
        };
    }
//
//    @Bean
//    public UserVOValidator beforeCreateUserVOValidator() {
//        return new UserVOValidator();
//    }
}
