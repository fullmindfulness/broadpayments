package pl.broadminds.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import pl.broadminds.cards.domain.Card;
import pl.broadminds.cards.repository.CardRepository;
import pl.broadminds.payments.domain.SystemParameter;
import pl.broadminds.payments.domain.SystemParameterName;
import pl.broadminds.payments.domain.User;
import pl.broadminds.payments.repository.SystemParameterRepository;
import pl.broadminds.payments.repository.UserRepository;
import pl.broadminds.payments.security.SpringSecurityAuthority;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import java.util.UUID;

@Configuration
@Slf4j
class InitalizeDatabase {

	@Value("${initialize.db}")
	private boolean initialize;

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CardRepository cardRepository;
	@Autowired
	private SystemParameterRepository systemParameterRepository;

	@Bean
	CommandLineRunner initDatabase() {

		return args -> {

			if (initialize) {

				SystemParameter commission = SystemParameter.builder()
						.name(SystemParameterName.COMMISSION)
						.value("0.05")
						.build();

				systemParameterRepository.save(commission);


				User admin = User.defaultValuesUserBuilder()
						.username("admin")
						.password(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode("supertajnehaslo"))
						.firstName("admin")
						.lastName("admin")
						.pesel("00000000000")
						.email("tester.oprogramowania.pro@gmail.com")
						.confirmationCode(UUID.randomUUID().toString())
						.confirmed(Boolean.TRUE)
						.active(Boolean.TRUE)
						.role(SpringSecurityAuthority.ROLE_ADMIN)
						.build();

				log.info("Preloading " + userRepository.save(admin));

				Card card = Card.builder()
						.number("1234567887654321")
						.expirationDate(LocalDate.of(2020, 12, 1))
						.currency(Currency.getInstance("PLN"))
						.csc(716)
						.balance(new BigDecimal(20000))
						.build();

				Card card2 = Card.builder()
						.number("1234123412341234")
						.expirationDate(LocalDate.of(2022, 5, 5))
						.currency(Currency.getInstance("USD"))
						.csc(333)
						.balance(new BigDecimal(50000))
						.build();

				Card card3 = Card.builder()
						.number("3333555577779999")
						.expirationDate(LocalDate.of(2021, 7, 7))
						.currency(Currency.getInstance("GBP"))
						.csc(909)
						.balance(new BigDecimal(100000))
						.build();

				log.info("Preloading " + cardRepository.save(card));
				log.info("Preloading " + cardRepository.save(card2));
				log.info("Preloading " + cardRepository.save(card3));
			}
		};
	}
}