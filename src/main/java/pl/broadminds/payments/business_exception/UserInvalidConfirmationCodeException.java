package pl.broadminds.payments.business_exception;

public class UserInvalidConfirmationCodeException extends PayMeException {

    public UserInvalidConfirmationCodeException(String msg) {
        super(msg);
    }
}