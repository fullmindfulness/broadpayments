package pl.broadminds.payments.business_exception;

public class UsernameNotFoundException extends PayMeException {
    public UsernameNotFoundException(String message) {
        super(message);
    }
}
