package pl.broadminds.payments.business_exception;

public class SystemParameterNotFoundException extends PayMeException {

    public SystemParameterNotFoundException(String message) {
        super(message);
    }
}
