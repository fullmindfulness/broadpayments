package pl.broadminds.payments.business_exception;

import lombok.Data;

@Data
public class PayMeException extends Exception {

    public PayMeException() {
        super();
    }

    public PayMeException(String message) {
        super(message);
    }
}
