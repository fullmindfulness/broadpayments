package pl.broadminds.payments.business_exception;

public class InvalidPaymentException extends PayMeException {
    public InvalidPaymentException(String message) {
        super(message);
    }
}
