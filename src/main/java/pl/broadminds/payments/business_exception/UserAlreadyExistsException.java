package pl.broadminds.payments.business_exception;

public class UserAlreadyExistsException extends PayMeException {
    public UserAlreadyExistsException(String msg) {
        super(msg);
    }
}
