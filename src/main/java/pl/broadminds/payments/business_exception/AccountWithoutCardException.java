package pl.broadminds.payments.business_exception;

public class AccountWithoutCardException extends PayMeException {

    public AccountWithoutCardException() {}

    public AccountWithoutCardException(String message) {
        super(message);
    }
}
