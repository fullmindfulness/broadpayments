package pl.broadminds.payments.business_exception;

public class AccountNotExistsException extends PayMeException {

    public AccountNotExistsException() {
        super();
    }

    public AccountNotExistsException(String message) {
        super(message);
    }
}
