package pl.broadminds.payments.business_exception;

public class AccountInsufficentFundsException extends PayMeException {

    public AccountInsufficentFundsException(String message) {
        super(message);
    }
}
