package pl.broadminds.payments.business_exception;

public class InvalidCurrencyException extends PayMeException {
    public InvalidCurrencyException(String message) {
        super(message);
    }
}
