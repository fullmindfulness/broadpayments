package pl.broadminds.payments.business_exception;

public class AccountAlreadyExistsExeption extends PayMeException {
    public AccountAlreadyExistsExeption(String message) {
        super(message);
    }
}
