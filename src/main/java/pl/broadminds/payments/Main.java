package pl.broadminds.payments;

import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.NumberValue;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.MonetaryConversions;

public class Main {

    public static void main(String[] args) {
        MonetaryAmount oneDollar = Monetary.getDefaultAmountFactory().setCurrency("USD").setNumber(1).create();
        NumberValue number = oneDollar.getNumber();
        CurrencyUnit usd = Monetary.getCurrency("USD");
        Money moneyof = Money.of(12.9999, usd);
        System.out.println(moneyof);

        CurrencyConversion conversionQuote = MonetaryConversions.getConversion("USD");
        MonetaryAmount converted = moneyof.with(conversionQuote);
        Money from = Money.from(converted);
        System.out.println(from);

    }
}
