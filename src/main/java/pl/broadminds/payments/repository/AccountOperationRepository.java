package pl.broadminds.payments.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.broadminds.payments.domain.AccountOperation;

@RepositoryRestResource(collectionResourceRel = "account-operations", path = "account-operations")
public interface AccountOperationRepository extends PagingAndSortingRepository<AccountOperation, Long> {

}
