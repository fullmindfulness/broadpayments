package pl.broadminds.payments.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.broadminds.payments.domain.User;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "users", path = "users")
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    List<User> findAllByFirstNameOrLastName(
            @Param("firstName") String firstName, @Param("lastName") String lastName);

    Optional<User> findOneByUsername(String login);

    Optional<User> findOneByConfirmationCode(String code);
}
