package pl.broadminds.payments.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.broadminds.payments.domain.SystemParameter;
import pl.broadminds.payments.domain.SystemParameterName;

import java.util.Optional;

@RepositoryRestResource(collectionResourceRel = "system-parameters", path = "system-parameters")
public interface SystemParameterRepository extends PagingAndSortingRepository<SystemParameter, Long> {

    Optional<SystemParameter> findOneByName(SystemParameterName name);

}
