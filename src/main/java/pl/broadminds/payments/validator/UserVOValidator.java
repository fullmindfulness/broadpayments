package pl.broadminds.payments.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.broadminds.payments.domain.vo.input.UserRegistrationData;


public class UserVOValidator implements Validator {
 
    @Override
    public boolean supports(Class<?> clazz) {
        return UserRegistrationData.class.equals(clazz);
    }
 
    @Override
    public void validate(Object obj, Errors errors) {
        UserRegistrationData user = (UserRegistrationData) obj;
        if (checkInputString(user.getFirstName())) {
            errors.rejectValue("firstName", "firstName.empty");
        }

        if (checkInputString(user.getLastName())) {
            errors.rejectValue("lastName", "lastName.empty");
        }

        if (checkInputString(user.getUsername())) {
            errors.rejectValue("username", "username.empty");
        }

        if (checkInputString(user.getPesel())) {
            errors.rejectValue("pesel", "pesel.empty");
        }
    }
 
    private boolean checkInputString(String input) {
        return (input == null || input.trim().length() == 0);
    }
}