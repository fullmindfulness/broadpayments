package pl.broadminds.payments.service;

import org.javamoney.moneta.Money;
import org.springframework.stereotype.Service;
import pl.broadminds.payments.domain.vo.ExchangedMoney;
import pl.broadminds.payments.service.contract.ICurrencyConversionService;

import javax.money.MonetaryAmount;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.ExchangeRate;
import javax.money.convert.MonetaryConversions;
import java.math.BigDecimal;
import java.util.Currency;

@Service
public class MockCurrencyConversionService implements ICurrencyConversionService {

    public BigDecimal convertAsBigDecimal(BigDecimal amount, Currency base, Currency quote) {
        return amount;
    }

    public ExchangedMoney convertAsMoney(Money money, Currency quote) {
        return ExchangedMoney.builder().money(money).rate(null).build();
    }
}

