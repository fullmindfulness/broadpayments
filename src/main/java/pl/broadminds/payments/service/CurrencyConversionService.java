package pl.broadminds.payments.service;

import org.javamoney.moneta.Money;
import org.springframework.stereotype.Service;
import pl.broadminds.payments.domain.vo.ExchangedMoney;
import pl.broadminds.payments.service.contract.ICurrencyConversionService;

import javax.money.MonetaryAmount;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.ExchangeRate;
import javax.money.convert.MonetaryConversions;
import java.math.BigDecimal;
import java.util.Currency;

@Service
public class CurrencyConversionService implements ICurrencyConversionService {

    public BigDecimal convertAsBigDecimal(BigDecimal amount, Currency base, Currency quote) {
        return convertAsMoney(Money.of(amount, base.getCurrencyCode()), quote).getMoney().getNumber().numberValue(BigDecimal.class);
    }

    public ExchangedMoney convertAsMoney(Money money, Currency quote) {
        CurrencyConversion conversionQuote = MonetaryConversions.getConversion(quote.getCurrencyCode());
        ExchangeRate exchangeRate =
                conversionQuote.getExchangeRateProvider().getExchangeRate(money.getCurrency().getCurrencyCode(), quote.getCurrencyCode());
        MonetaryAmount converted = money.with(conversionQuote);
        return ExchangedMoney.builder().money(Money.from(converted)).rate(exchangeRate).build();
    }
}

