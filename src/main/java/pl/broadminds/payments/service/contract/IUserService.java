package pl.broadminds.payments.service.contract;

import pl.broadminds.cards.domain.Card;
import pl.broadminds.payments.business_exception.PayMeException;
import pl.broadminds.payments.domain.Account;
import pl.broadminds.payments.domain.User;
import pl.broadminds.payments.domain.vo.input.CardAssigmentData;
import pl.broadminds.payments.domain.vo.output.PaymentResult;
import pl.broadminds.payments.domain.vo.input.UserRegistrationData;

import java.math.BigDecimal;
import java.util.Currency;


public interface IUserService {

    User register(UserRegistrationData user) throws PayMeException;

    User findUser(String username) throws PayMeException;

    User confirmByCode(String code) throws PayMeException;

    User confirmByUsername(String username) throws PayMeException;

    Account setCard(String username, CardAssigmentData card) throws PayMeException;

    Account openAccount(String username, Currency instance) throws PayMeException;

    Account closeAccount(String username) throws PayMeException;

    Account creditAccount(String username, BigDecimal amount, Integer csc) throws PayMeException;

    Account debitAccount(String username, BigDecimal amount, Integer csc) throws PayMeException;

    Account changeAccountCurrency(String username, Currency currency) throws PayMeException;

    PaymentResult pay(String debtorUsername, String creditorUsername, BigDecimal amount, String description) throws PayMeException;


}
