package pl.broadminds.payments.service.contract;

public interface IMailService {
    void sendSimpleMessage(String to, String subject, String text);
}
