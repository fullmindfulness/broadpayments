package pl.broadminds.payments.service.contract;

import pl.broadminds.cards.domain.Card;
import pl.broadminds.payments.business_exception.AccountNotExistsException;
import pl.broadminds.payments.business_exception.PayMeException;
import pl.broadminds.payments.domain.Account;
import pl.broadminds.payments.domain.vo.input.CardAssigmentData;
import pl.broadminds.payments.domain.vo.output.PaymentResult;

import java.math.BigDecimal;
import java.util.Currency;

public interface IAccountService {

    Account changeAccountCurrency(Account account, Currency currency) throws PayMeException;

    Account closeAccount(Account account) throws PayMeException;

    Account activateAccount(Account account) throws AccountNotExistsException;

    Account suspendAccount(Account account) throws PayMeException;

    Account credit(Account account, BigDecimal amount, Integer csc) throws PayMeException;

    Account debit(Account account, BigDecimal amount, Integer csc) throws PayMeException;

    PaymentResult pay(Account debitedAccount, Account creditedAccount, BigDecimal amount, String description) throws PayMeException;

    Account setCard(Account account, CardAssigmentData card) throws PayMeException;
}
