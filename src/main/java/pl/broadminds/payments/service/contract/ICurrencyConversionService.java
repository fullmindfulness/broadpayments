package pl.broadminds.payments.service.contract;

import org.javamoney.moneta.Money;
import pl.broadminds.payments.domain.vo.ExchangedMoney;

import java.math.BigDecimal;
import java.util.Currency;

public interface ICurrencyConversionService {

    ExchangedMoney convertAsMoney(Money money, Currency quote);

    BigDecimal convertAsBigDecimal(BigDecimal amount, Currency base, Currency quote);

}
