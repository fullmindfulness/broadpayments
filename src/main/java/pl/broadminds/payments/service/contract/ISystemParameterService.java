package pl.broadminds.payments.service.contract;

import pl.broadminds.payments.business_exception.PayMeException;

import java.math.BigDecimal;

public interface ISystemParameterService {

    BigDecimal getCommission() throws PayMeException;
}
