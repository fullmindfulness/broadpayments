package pl.broadminds.payments.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.broadminds.payments.business_exception.PayMeException;
import pl.broadminds.payments.business_exception.SystemParameterNotFoundException;
import pl.broadminds.payments.domain.SystemParameterName;
import pl.broadminds.payments.repository.SystemParameterRepository;
import pl.broadminds.payments.service.contract.ISystemParameterService;

import java.math.BigDecimal;

@Service
public class SystemParameterService implements ISystemParameterService {

    @Autowired
    private SystemParameterRepository systemParameterRepository;

    public BigDecimal getCommission() throws PayMeException {
        String systemParameterValue = systemParameterRepository.findOneByName(SystemParameterName.COMMISSION)
                .orElseThrow(() -> new SystemParameterNotFoundException(SystemParameterName.COMMISSION.name()))
                .getValue();
        return new BigDecimal(systemParameterValue);
    }
}
