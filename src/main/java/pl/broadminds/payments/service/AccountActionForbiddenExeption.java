package pl.broadminds.payments.service;

import pl.broadminds.payments.business_exception.PayMeException;

public class AccountActionForbiddenExeption extends PayMeException {
    public AccountActionForbiddenExeption(String message) {
        super(message);
    }
}
