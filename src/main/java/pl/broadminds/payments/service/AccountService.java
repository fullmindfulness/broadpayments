package pl.broadminds.payments.service;

import org.javamoney.moneta.Money;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.broadminds.cards.domain.Card;
import pl.broadminds.cards.service.ICardService;
import pl.broadminds.payments.business_exception.AccountInsufficentFundsException;
import pl.broadminds.payments.business_exception.AccountNotExistsException;
import pl.broadminds.payments.business_exception.AccountWithoutCardException;
import pl.broadminds.payments.business_exception.PayMeException;
import pl.broadminds.payments.domain.Account;
import pl.broadminds.payments.domain.AccountOperation;
import pl.broadminds.payments.domain.AccountOperationStatus;
import pl.broadminds.payments.domain.AccountOperationType;
import pl.broadminds.payments.domain.vo.ExchangedMoney;
import pl.broadminds.payments.domain.vo.input.CardAssigmentData;
import pl.broadminds.payments.domain.vo.output.PaymentResult;
import pl.broadminds.payments.repository.AccountOperationRepository;
import pl.broadminds.payments.repository.AccountRepository;
import pl.broadminds.payments.service.contract.IAccountService;
import pl.broadminds.payments.service.contract.ICurrencyConversionService;
import pl.broadminds.payments.service.contract.ISystemParameterService;

import javax.money.convert.ExchangeRate;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;

@Service
public class AccountService implements IAccountService {

    @Autowired
    private ICardService cardService;

    @Autowired
    private ICurrencyConversionService currencyConversionService;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountOperationRepository accountOperationRepository;

    @Autowired
    private ISystemParameterService systemParameterService;

    @Override
    public Account changeAccountCurrency(Account account, Currency currency) throws PayMeException {
        verifyAccount(account);
        final Money balanceMoney = Money.of(account.getBalance(), account.getCurrency().getCurrencyCode());
        final ExchangedMoney convertedBalanceExchangedMoney = currencyConversionService.convertAsMoney(balanceMoney, currency);
        final Money convertedBalanceMoney = convertedBalanceExchangedMoney.getMoney();
        final ExchangeRate convertedBalanceExchangeRate = convertedBalanceExchangedMoney.getRate();

        account.convert(convertedBalanceMoney);
        saveAccountOperation(AccountOperationType.DEBIT, account, balanceMoney, convertedBalanceMoney,
                convertedBalanceExchangeRate, BigDecimal.ZERO, "Obciążenie - zmiana waluty konta");
        saveAccountOperation(AccountOperationType.CREDIT, account, convertedBalanceMoney, balanceMoney,
                convertedBalanceExchangeRate, account.getBalance(), "Uznanie - zmiana waluty konta");
        return account;
    }

    @Override
    public Account closeAccount(Account account) throws PayMeException {
        verifyForDebitTransfer(account, account.getBalance());
        final Money accountDebitMoney = Money.of(account.getBalance(), account.getCurrency().getCurrencyCode());
        final ExchangedMoney cardCreditExchangedMoney = currencyConversionService.convertAsMoney(accountDebitMoney, account.getCard().getCurrency());
        final Money cardCreditMoney = cardCreditExchangedMoney.getMoney();
        final ExchangeRate cardCreditExchangeRate = cardCreditExchangedMoney.getRate();

        cardService.credit(account.getCard().getNumber(), account.getCard().getCsc(), cardCreditMoney);
        saveAccountOperation(AccountOperationType.DEBIT, account, accountDebitMoney, cardCreditMoney,
                cardCreditExchangeRate, account.getBalance(), "Transfer wychodzący - zamknięcie konta");
        return account.close();
    }

    @Override
    public Account credit(Account account, BigDecimal amount, Integer csc) throws PayMeException {
        verifyForCreditTransfer(account);
        final Money accountCreditMoney = Money.of(amount, account.getCurrency().getCurrencyCode());
        final ExchangedMoney cardDebitExchangedMoney = currencyConversionService.convertAsMoney(accountCreditMoney, account.getCard().getCurrency());
        final Money cardDebitMoney = cardDebitExchangedMoney.getMoney();
        final ExchangeRate cardDebitExchangeRate = cardDebitExchangedMoney.getRate();

        account.credit(accountCreditMoney);
        cardService.debit(account.getCard().getNumber(), csc, cardDebitMoney);
        saveAccountOperation(AccountOperationType.CREDIT, account, accountCreditMoney,
                cardDebitMoney, cardDebitExchangeRate, account.getBalance(), "Transfer przychodzący - zasilenie konta z karty");
        return account;
    }

    @Override
    public Account debit(Account account, BigDecimal amount, Integer csc) throws PayMeException {
        verifyForDebitTransfer(account, amount);
        final Money accountDebitMoney = Money.of(amount, account.getCurrency().getCurrencyCode());
        final ExchangedMoney cardCreditExchangedMoney = currencyConversionService.convertAsMoney(accountDebitMoney, account.getCard().getCurrency());
        final Money cardCreditMoney = cardCreditExchangedMoney.getMoney();
        final ExchangeRate cardCreditExchangeRate = cardCreditExchangedMoney.getRate();

        account.debit(accountDebitMoney);
        cardService.credit(account.getCard().getNumber(), csc, cardCreditMoney);
        saveAccountOperation(AccountOperationType.DEBIT, account, accountDebitMoney, cardCreditMoney,
                cardCreditExchangeRate, account.getBalance(), "Transfer wychodzący - przelew środków na kartę");
        return account;
    }

    @Override
    public PaymentResult pay(Account debitedAccount, Account creditedAccount, BigDecimal amount, String description) throws PayMeException {
        verifyForDebitTransfer(debitedAccount, amount);
        verifyForCreditTransfer(creditedAccount);

        Money creditedAccountMoney = Money.of(amount, creditedAccount.getCurrency().getCurrencyCode());
        final ExchangedMoney debitedAccountExchangedMoney =
                currencyConversionService.convertAsMoney(creditedAccountMoney, debitedAccount.getCard().getCurrency());
        final Money debitedAccountMoney = debitedAccountExchangedMoney.getMoney();
        final ExchangeRate debitedAccountExchangeRate = debitedAccountExchangedMoney.getRate();

        // TODO charge commission
        final BigDecimal commissionValue = systemParameterService.getCommission();
        final Money commissionMoney = creditedAccountMoney.multiply(commissionValue);
        //debitedAccountMoney = debitedAccountMoney.add(commissionMoney);

        debitedAccount.debit(debitedAccountMoney);
        creditedAccount.credit(creditedAccountMoney);
        saveAccountOperation(AccountOperationType.DEBIT, debitedAccount, debitedAccountMoney, creditedAccountMoney,
                debitedAccountExchangeRate, debitedAccount.getBalance(), "Płatność wychodząca");
        saveAccountOperation(AccountOperationType.CREDIT, creditedAccount, creditedAccountMoney, debitedAccountMoney,
                debitedAccountExchangeRate, creditedAccount.getBalance(), "Płatność przychodząca");

        creditedAccountMoney = creditedAccountMoney.subtract(commissionMoney);


        return PaymentResult.builder()
                .debtor(debitedAccount.getUser().getUsername())
                .creditor(creditedAccount.getUser().getUsername())
                .description(description)
                .debitedAmount(debitedAccountMoney.getNumber().numberValue(BigDecimal.class))
                .debitedCurrency(Currency.getInstance(debitedAccountMoney.getCurrency().getCurrencyCode()))
                .creditedAmount(creditedAccountMoney.getNumber().numberValue(BigDecimal.class))
                .creditedCurrency(Currency.getInstance(creditedAccountMoney.getCurrency().getCurrencyCode()))
                .exchangeRate(debitedAccountExchangeRate.getFactor().numberValue(BigDecimal.class))
                .commissionValue(commissionValue)
                .commissionAmount(commissionMoney.getNumber().numberValue(BigDecimal.class))
                .commissionCurrency(Currency.getInstance(commissionMoney.getCurrency().getCurrencyCode()))
                .build();
    }

    @Override
    public Account setCard(Account account, CardAssigmentData cardAssigmentData) throws PayMeException {
        verifyAccount(account);
        final Card card = cardService.findValidCard(cardAssigmentData.getNumber(),
                cardAssigmentData.getCsc(), cardAssigmentData.getExpirationDate());
        account.setCard(card);
        accountRepository.save(account);
        return account;
    }

    private void verifyAccount(Account account) throws PayMeException {
        if (account == null)
            throw new AccountNotExistsException();
        if (!account.isActive())
            throw new AccountActionForbiddenExeption("Status konta nie pozwala na wykonanie operacji: " + account.getStatus());
    }

    private void verifyForTransfer(Account account) throws PayMeException {
        this.verifyAccount(account);
        if (account.getCard() == null)
            throw new AccountWithoutCardException();
    }

    private void verifyForCreditTransfer(Account account) throws PayMeException {
        verifyForTransfer(account);
    }

    private void verifyForDebitTransfer(Account account, BigDecimal amount) throws PayMeException {
        verifyForTransfer(account);
        if (!account.hasEnoughFunds(amount))
            throw new AccountInsufficentFundsException(account.getBalance().toString());
    }

    private void saveAccountOperation(AccountOperationType type, Account account,
                                      Money debitedMoney, Money creditedMoney,
                                      ExchangeRate exchangeRate, BigDecimal balance, String description) {
        AccountOperation accountOperation = AccountOperation.builder()
                .operationType(type)
                .operationStatus(AccountOperationStatus.EXECUTED)
                .account(account)
                .amount(debitedMoney.getNumber().numberValue(BigDecimal.class))
                .currency(Currency.getInstance(debitedMoney.getCurrency().getCurrencyCode()))
                .otherOperationSideAmount(creditedMoney.getNumber().numberValue(BigDecimal.class))
                .otherOperationSideCurrency(Currency.getInstance(creditedMoney.getCurrency().getCurrencyCode()))
                .exchangeRate(exchangeRate.getFactor().numberValue(BigDecimal.class))
                .balance(balance)
                .time(Instant.now())
                .description(description)
                .build();
        accountOperationRepository.save(accountOperation);
    }

    @Override
    public Account activateAccount(Account account) throws AccountNotExistsException {
        if (account == null)
            throw new AccountNotExistsException();
        account.activate();
        return accountRepository.save(account);
    }

    @Override
    public Account suspendAccount(Account account) throws AccountNotExistsException {
        if (account == null)
            throw new AccountNotExistsException();
        account.suspend();
        return accountRepository.save(account);
    }
}
