package pl.broadminds.payments.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.stereotype.Service;
import pl.broadminds.payments.business_exception.*;
import pl.broadminds.payments.domain.Account;
import pl.broadminds.payments.domain.AccountStatus;
import pl.broadminds.payments.domain.User;
import pl.broadminds.payments.domain.vo.input.CardAssigmentData;
import pl.broadminds.payments.domain.vo.output.PaymentResult;
import pl.broadminds.payments.domain.vo.input.UserRegistrationData;
import pl.broadminds.payments.repository.AccountRepository;
import pl.broadminds.payments.repository.UserRepository;
import pl.broadminds.payments.service.contract.IAccountService;
import pl.broadminds.payments.service.contract.IMailService;
import pl.broadminds.payments.service.contract.IUserService;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.UUID;

@Service
public class UserService implements IUserService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private IAccountService accountService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private IMailService mailService;

    @Override
    public User register(UserRegistrationData userRegistrationData) throws PayMeException {
        if (userRepository.findOneByUsername(userRegistrationData.getUsername()).isPresent())
            throw new UserAlreadyExistsException(userRegistrationData.getUsername());
        User user = User.defaultValuesUserBuilder()
                .username(userRegistrationData.getUsername())
                .password(PasswordEncoderFactories.createDelegatingPasswordEncoder().encode(userRegistrationData.getPassword()))
                .firstName(userRegistrationData.getFirstName())
                .lastName(userRegistrationData.getLastName())
                .pesel(userRegistrationData.getPesel())
                .email(userRegistrationData.getEmail())
                .confirmationCode(UUID.randomUUID().toString())
                .build();
        userRepository.save(user);
//        mailService.sendSimpleMessage("toprobm@gmail.com", "Rejestracja konta",
//                "APP-URL\\" + userRegistrationData.getConfirmationCode());
        return user;
    }

    @Override
    public User confirmByCode(String code) throws PayMeException {
        User user = userRepository.findOneByConfirmationCode(code)
                .orElseThrow(() -> new UserInvalidConfirmationCodeException(code));
        user.confirm();
        userRepository.save(user);
        return user;
    }

    @Override
    public User confirmByUsername(String username) throws PayMeException {
        User user = findUser(username);
        user.confirm();
        userRepository.save(user);
        return user;
    }

    @Override
    public Account openAccount(String username, Currency currency) throws PayMeException {
        User user = findUser(username);
        if (user.getAccount() != null)
            throw new AccountAlreadyExistsExeption(username);
        Account account = Account.builder()
                .user(user)
                .currency(currency)
                .balance(BigDecimal.ZERO)
                .status(AccountStatus.ACTIVE)
                .build();
        user.setAccount(account);
        return accountRepository.save(account);
    }

    @Override
    public User findUser(String username) throws UsernameNotFoundException {
        return userRepository.findOneByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
    }

    @Override
    public Account closeAccount(String username) throws PayMeException {
        Account account = findUser(username).getAccount();
        return accountService.closeAccount(account);
    }

    @Override
    public Account creditAccount(String username, BigDecimal amount, Integer csc) throws PayMeException {
        Account account = findUser(username).getAccount();
        return accountService.credit(account, amount, csc);
    }

    @Override
    public Account debitAccount(String username, BigDecimal amount, Integer csc) throws PayMeException {
        Account account = findUser(username).getAccount();
        return accountService.debit(account, amount, csc);
    }

    @Override
    public PaymentResult pay(String debtorUsername, String creditorUsername, BigDecimal amount, String description) throws PayMeException {
        User debtor = findUser(debtorUsername);
        User creditor = findUser(creditorUsername);
        return accountService.pay(debtor.getAccount(), creditor.getAccount(), amount, description);
    }

    @Override
    public Account changeAccountCurrency(String username, Currency currency) throws PayMeException {
        Account account = findUser(username).getAccount();
        return accountService.changeAccountCurrency(account, currency);
    }

    @Override
    public Account setCard(String username, CardAssigmentData card) throws PayMeException {
        Account account = findUser(username).getAccount();
        return accountService.setCard(account, card);
    }
}
