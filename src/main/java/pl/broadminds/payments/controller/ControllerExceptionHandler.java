package pl.broadminds.payments.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.broadminds.cards.business_exception.CardExpiredException;
import pl.broadminds.cards.business_exception.CardNotFoundException;
import pl.broadminds.cards.service.CardInsufficentEnoughFundsException;
import pl.broadminds.payments.business_exception.*;
import pl.broadminds.payments.service.AccountActionForbiddenExeption;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static pl.broadminds.payments.controller.ErrorMessages.USER_ALREADY_EXISTS;

@ControllerAdvice
public class ControllerExceptionHandler {

    @ExceptionHandler(InvalidCurrencyException.class)
    public void handleInvalidCurrencyException(HttpServletResponse response, InvalidCurrencyException ex) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), "Nieprawidłowa waluta: " + ex.getMessage());
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public void handleUsernameNotFoundException(HttpServletResponse response, UsernameNotFoundException ex) throws IOException {
        // TODO - error on purpose, different message
//        response.sendError(HttpStatus.NOT_FOUND.value(), "Niepoprawna nazwa użytkownika: " + ex.getMessage());
        response.sendError(HttpStatus.BAD_REQUEST.value(), "Nie odnaleziono: " + ex.getMessage());
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    public void handleUserAlreadyExistsException(HttpServletResponse response, UserAlreadyExistsException ex) throws IOException {
        // TODO - error on purpose, different status code
//        response.sendError(HttpStatus.BAD_REQUEST.value(), "USER_ALREADY_EXISTS " + ex.getMessage());
        response.sendError(HttpStatus.NOT_FOUND.value(), USER_ALREADY_EXISTS + ex.getMessage());
    }

    @ExceptionHandler(UserInvalidConfirmationCodeException.class)
    public void handleUserInvalidConfirmationCodeException(HttpServletResponse response, UserInvalidConfirmationCodeException ex) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value(), "Niepoprawny kod potwierdzający: " + ex.getMessage());
    }

    @ExceptionHandler(CardNotFoundException.class)
    public void handleCardNotFoundException(HttpServletResponse response, CardNotFoundException ex) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), "Niepoprawne dane karty nr: " + ex.getMessage());
    }

    @ExceptionHandler(CardExpiredException.class)
    public void handleCardExpiredException(HttpServletResponse response, CardExpiredException ex) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), "Upłynęła data ważności karty nr: " + ex.getMessage());
    }

    @ExceptionHandler(CardInsufficentEnoughFundsException.class)
    public void handleCardNotEnoughFundsException(HttpServletResponse response, CardInsufficentEnoughFundsException ex) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), "Brak wystarczających środków na karcie: " + ex.getMessage());
    }

    @ExceptionHandler(AccountActionForbiddenExeption.class)
    public void handleAccountActionForbidden(HttpServletResponse response, AccountActionForbiddenExeption ex) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), "Akcja niedozwolona: " + ex.getMessage());
    }

    @ExceptionHandler(AccountNotExistsException.class)
    public void handleAccountNotExists(HttpServletResponse response, AccountNotExistsException ex) throws IOException {
        response.sendError(HttpStatus.NOT_FOUND.value(), "Konto nie istnieje.");
    }

    @ExceptionHandler(AccountInsufficentFundsException.class)
    public void handleAccountNotEnoughFundsException(HttpServletResponse response, AccountInsufficentFundsException ex) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), "Brak wystarczających środków na koncie: " + ex.getMessage());
    }

    @ExceptionHandler(AccountWithoutCardException.class)
    public void handleAccountWithoutCardException(HttpServletResponse response, AccountWithoutCardException ex) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), "Konto nie posiada powiązanej karty: " + ex.getMessage());
    }

    @ExceptionHandler(SystemParameterNotFoundException.class)
    public void handleSystemParameterNotFoundException(HttpServletResponse response, SystemParameterNotFoundException ex) throws IOException {
        response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Parametr systemu nie został odnaleziony: " + ex.getMessage());
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return ResponseEntity.badRequest().body(errors);
    }
}
