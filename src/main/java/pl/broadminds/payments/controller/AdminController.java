package pl.broadminds.payments.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.broadminds.payments.business_exception.PayMeException;
import pl.broadminds.payments.domain.Account;
import pl.broadminds.payments.domain.User;
import pl.broadminds.payments.service.contract.IAccountService;
import pl.broadminds.payments.service.contract.IUserService;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@RestController
//@Validated
@Api(description = "Admin API", protocols = "https", authorizations = @Authorization("Basic"))
public class AdminController {

    @Autowired
    private IAccountService accountService;

    @Autowired
    private IUserService userService;

    @ApiOperation(value = "Confirm user registration.", response = User.class, position = 0)
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "User registration confirmed.",
                    response = User.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 501, message = "Not Implemented")
    })
    @PostMapping("/admin/user/confirm/{username}")
    public ResponseEntity<User>  confirm(
            @ApiParam(value = "Username of the user to confirm user registration.", required = true)
            @NotEmpty
            @PathVariable String username) throws PayMeException {
        final User confirmed = userService.confirmByUsername(username);
        return ResponseEntity.ok(confirmed);
    }

    @ApiOperation(value = "Suspend user's money account.", response = Account.class, position = 1)
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Money account suspended.",
                    response = Account.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 501, message = "Not Implemented")
    })
    @PostMapping("/admin/account/suspend/{username}")
    public ResponseEntity<Account>  suspendAccount(
            @ApiParam(value = "Username of the user whose money account is to be suspended.", required = true)
            @NotEmpty
            @PathVariable String username) throws PayMeException {
        final Account account = userService.findUser(username).getAccount();
        accountService.suspendAccount(account);
        return ResponseEntity.ok(account);
    }


    @ApiOperation(value = "Activate user's money account.", response = Account.class, position = 0)
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Money account activated.",
                    response = Account.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 501, message = "Not Implemented")
    })
    @PostMapping("/admin/account/activate/{username}")
    public ResponseEntity<Account> activateAccount(
            @ApiParam(value = "Username of the user whose money account is to be activated.", required = true)
            @NotNull
            @PathVariable String username) throws PayMeException {
        final Account account = userService.findUser(username).getAccount();
        accountService.activateAccount(account);
        return ResponseEntity.ok(account);
    }


    // TODO
//    @PostMapping(value = "/admin/exchange-rates/load", consumes = {"multipart/form-data"})
//    public UploadedFileVO loadExchangeRates(@RequestParam MultipartFile file) throws IOException {
//        String fileContent = new String(file.getBytes());
////        BaseExchangeRate baseExchangeRate = new ObjectMapper().getJson
//        return new UploadedFileVO();
//    }


}
