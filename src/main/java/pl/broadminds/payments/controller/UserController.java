package pl.broadminds.payments.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import pl.broadminds.cards.business_exception.CardNotFoundException;
import pl.broadminds.cards.domain.Card;
import pl.broadminds.payments.business_exception.AccountNotExistsException;
import pl.broadminds.payments.business_exception.InvalidCurrencyException;
import pl.broadminds.payments.business_exception.PayMeException;
import pl.broadminds.payments.domain.Account;
import pl.broadminds.payments.domain.User;
import pl.broadminds.payments.domain.vo.input.CardAssigmentData;
import pl.broadminds.payments.domain.vo.input.PaymentRequest;
import pl.broadminds.payments.domain.vo.output.PaymentResult;
import pl.broadminds.payments.domain.vo.input.TransferRequest;
import pl.broadminds.payments.service.contract.IUserService;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.Currency;

@RestController
@Api(description = "User API", protocols = "http", authorizations = @Authorization("Basic"))
//@Validated
public class UserController {

    @Autowired
    private IUserService userService;

    @ApiOperation(value = "Retrieve full data of the user profile and money account along with history of operations.",
            response = User.class,
            position = 0)
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "OK",
                    response = User.class),
            @ApiResponse(code = 400, message = "Bad Request."),
            @ApiResponse(code = 501, message = "Not Implemented")
    })
    @GetMapping(value = "/user/profile")
    public ResponseEntity<User> profile(@ApiIgnore Principal principal) throws PayMeException {
        final User user = userService.findUser(principal.getName());
        return ResponseEntity.ok(user);
    }

    @ApiOperation(value = "Retrieve data of the user's money account along with history of operations.",
            response = Account.class,
            position = 1)
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "OK",
                    response = Account.class),
            @ApiResponse(code = 400, message = "Bad Request."),
            @ApiResponse(code = 501, message = "Not Implemented")
    })
    @GetMapping(value = "/user/account")
    public ResponseEntity<Account> account(@ApiIgnore Principal principal) throws PayMeException {
        final User user = userService.findUser(principal.getName());
        if (user.getAccount() == null)
            throw new AccountNotExistsException();
        return ResponseEntity.ok(user.getAccount());
    }

    @ApiOperation(value = "Retrieve data of the card assigned to user's money account along with history of operations.",
            response = Card.class,
            position = 2)
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "OK",
                    response = Card.class),
            @ApiResponse(code = 400, message = "Bad Request."),
            @ApiResponse(code = 501, message = "Not Implemented")
    })
    @GetMapping(value = "/user/account/card")
    public ResponseEntity<Card> card(@ApiIgnore Principal principal) throws PayMeException {
        final User user = userService.findUser(principal.getName());
        if (user.getAccount() == null)
            throw new AccountNotExistsException();
        if (user.getAccount().getCard() == null)
            throw new CardNotFoundException("");
        return ResponseEntity.ok(user.getAccount().getCard());
    }

    @ApiOperation(value = "Open user's money account in specified currency.",
            response = Account.class,
            code = 201,
            position = 3)
    @ApiResponses({
            @ApiResponse(
                    code = 201,
                    message = "Created, hyperlink to new user's money account in Location header",
                    response = Account.class,
                    responseHeaders = {
                            @ResponseHeader(name = "Location", description = "Hyperlink to user's money account opened.", response = String.class)
                    }),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 501, message = "Not Implemented")
    })
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/user/account/open/{currencyCode}")
    public ResponseEntity<Account> openAccount(
            @ApiParam(value = "Currency code for user's money account to be created.", required = true)
            @NotNull
            @PathVariable String currencyCode,
            @ApiIgnore Principal principal, UriComponentsBuilder builder) throws PayMeException {
        try {
            final Currency currency = Currency.getInstance(currencyCode);
            final Account account = userService.openAccount(principal.getName(), currency);
            final UriComponents uriComponents = builder.path("/user/account").buildAndExpand();
            return ResponseEntity.created(uriComponents.toUri()).body(account);
        } catch (IllegalArgumentException ex) {
            throw new InvalidCurrencyException(currencyCode);
        }
    }

    @ApiOperation(value = "Change user's money account currency.", response = Account.class, position = 4)
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "OK",
                    response = Account.class),
            @ApiResponse(code = 400, message = "Bad Request. {custom _ message}"),
            @ApiResponse(code = 501, message = "Not Implemented")
    })
    @PostMapping("/user/account/convert/{currencyCode}")
    public ResponseEntity<Account> changeAccountCurrency(
            @ApiParam(value = "New currency code for user's money account.", required = true)
            @NotNull
            @PathVariable String currencyCode,
            @ApiIgnore Principal principal) throws PayMeException {
        try {
            final Currency currency = Currency.getInstance(currencyCode);
            final Account account = userService.changeAccountCurrency(principal.getName(), currency);
            // TODO Zmiana waluty konta dokonuje przeliczenia kwoty, ale nie zmienia waluty konta,
            // TODO i nie zwraca operacji na liście operacji (poza transakcją w controllerze, nie będzie flush)
            account.getOperations().remove(account.getOperations().size() - 1);
            return ResponseEntity.ok().body(account);
        } catch (IllegalArgumentException ex) {
            throw new InvalidCurrencyException(currencyCode);
        }
    }

    @ApiOperation(value = "Close user's money account.", response = Account.class, position = 5)
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "OK",
                    response = Account.class),
            @ApiResponse(code = 400, message = "Bad Request."),
            @ApiResponse(code = 501, message = "Not Implemented")
    })
    @PostMapping("/user/account/close")
    public ResponseEntity<Account> closeAccount(@ApiIgnore Principal principal) throws PayMeException {
        final Account account = userService.closeAccount(principal.getName());
        return ResponseEntity.ok().body(account);
    }

    @ApiOperation(value = "Assign card to user's money account.", response = Account.class, position = 6)
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Card assigned.",
                    response = Account.class),
            @ApiResponse(code = 400, message = "Bad Request", response = ApiError.class),
            @ApiResponse(code = 501, message = "Not Implemented")
    })
    @PostMapping("/user/account/card/set")
    public ResponseEntity<Account> setCard(@RequestBody @Valid CardAssigmentData card,
                                           @ApiIgnore Principal principal) throws PayMeException {
        Account account = userService.setCard(principal.getName(), card);
        return ResponseEntity.ok().body(account);
    }

    @ApiOperation(value = "Credit account using assigned card with specified amount.",
            response = Account.class, position = 7)
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Credit operation successful.",
                    response = Account.class),
            @ApiResponse(code = 400, message = "Bad Request", response = ApiError.class),
            @ApiResponse(code = 501, message = "Not Implemented")
    })
    @PostMapping("/user/account/credit")
    public ResponseEntity<Account> credit(@RequestBody @Valid TransferRequest transferRequest, @ApiIgnore Principal principal) throws PayMeException {
        Account account = userService.creditAccount(principal.getName(), transferRequest.getAmount(), transferRequest.getCsc());
        return ResponseEntity.ok().body(account);
    }

    @ApiOperation(value = "Debit account using assigned card with specified amount.",
            response = Account.class, position = 8)
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Debit operation successful.",
                    response = Account.class),
            @ApiResponse(code = 400, message = "Bad Request", response = ApiError.class),
            @ApiResponse(code = 501, message = "Not Implemented")
    })
    @PostMapping("/user/account/debit")
    public ResponseEntity<Account> debit(@RequestBody @Valid TransferRequest transferRequest, @ApiIgnore Principal principal) throws PayMeException {
        Account account = userService.debitAccount(principal.getName(), transferRequest.getAmount(), transferRequest.getCsc());
        return ResponseEntity.ok().body(account);
    }

    @ApiOperation(value = "Execute payment with specified amount to specified user.",
            response = PaymentResult.class, position = 9)
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "Payment operation successful.",
                    response = PaymentResult.class),
            @ApiResponse(code = 400, message = "Bad Request", response = ApiError.class),
            @ApiResponse(code = 501, message = "Not Implemented")
    })
    @PostMapping("/user/pay")
    public ResponseEntity<PaymentResult> pay(@RequestBody @Valid PaymentRequest payment, @ApiIgnore Principal principal) throws PayMeException {
        PaymentResult paymentResult =
                userService.pay(principal.getName(), payment.getCreditorUsername(), payment.getAmount(), payment.getDescription());
        return ResponseEntity.ok().body(paymentResult);
    }



}
