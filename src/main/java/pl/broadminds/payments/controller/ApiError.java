package pl.broadminds.payments.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.http.HttpStatus;


import java.nio.file.Path;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ApiError {

    @ApiModelProperty(position = 0)
    private ZonedDateTime timestamp;

    @ApiModelProperty(position = 1)
    private int status;

    @ApiModelProperty(position = 2)
    private String error;

    @ApiModelProperty(position = 3)
    private String message;

    @ApiModelProperty(position = 4)
    private String path;

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    //
//    ApiError(HttpStatus status, String message, Path path) {
//        this.timestamp = Instant.now().atZone(ZoneId.of("ECT"));
//        this.status = status.value();
//        this.error = status.getReasonPhrase();
//        this.message = message != null ? message : status.getReasonPhrase();
//
//
//    }


}
