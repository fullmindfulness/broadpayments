package pl.broadminds.payments.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import pl.broadminds.payments.business_exception.PayMeException;
import pl.broadminds.payments.domain.User;
import pl.broadminds.payments.domain.vo.input.UserRegistrationData;
import pl.broadminds.payments.service.contract.IUserService;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

@RestController
@RequestMapping("/public")
@Api(description = "Public API", protocols = "http")
//@Validated
public class PublicController {

    @Autowired
    private IUserService userService;

    @ApiOperation(value = "Register new user.", response = User.class, code = 201, position = 1)
    @ApiResponses({
            @ApiResponse(
                    code = 201,
                    message = "Created, hyperlink to new user profile is in Location header",
                    response = User.class,
                    responseHeaders = {
                            @ResponseHeader(name = "Location", description = "Hyperlink to new user profile created", response = String.class)
                    }),
            @ApiResponse(code = 400, message = "Bad Request", response = ApiError.class),
            @ApiResponse(code = 501, message = "Not Implemented")
    })
    @PostMapping("/user/register")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<User> register(
            @ApiParam(value = "User data for registration.", required = true)
            @RequestBody
            @Valid UserRegistrationData user,
            UriComponentsBuilder builder) throws PayMeException {
        final User registered = userService.register(user);
        final UriComponents uriComponents = builder.path("/user/register").buildAndExpand();
        return ResponseEntity.created(uriComponents.toUri()).body(registered);
    }

    @ApiOperation(value = "Confirm user registration.", response = User.class, position = 2)
    @ApiResponses({
            @ApiResponse(
                    code = 200,
                    message = "User registration confirmed.",
                    response = User.class),
            @ApiResponse(code = 400, message = "Bad Request", response = ApiError.class),
            @ApiResponse(code = 501, message = "Not Implemented")
    })
    @PostMapping("/user/confirm/{code}")
    public ResponseEntity<User> confirm(@ApiParam(value = "Code to confirm user registration.", required = true)
                                        @NotEmpty
                                        @PathVariable String code) throws PayMeException {
        final User confirmed = userService.confirmByCode(code);
        return ResponseEntity.ok(confirmed);
    }

}
