package pl.broadminds.payments.controller;

public class ErrorMessages {

    public static final String NOT_FOUND = "Nie odnaleziono: ";

    public static final String USER_ALREADY_EXISTS = "Podany użytkownik już istnieje: ";

}
