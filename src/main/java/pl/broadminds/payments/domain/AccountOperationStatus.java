package pl.broadminds.payments.domain;

public enum AccountOperationStatus {
    ON_HOLD, EXECUTED
}
