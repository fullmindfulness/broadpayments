package pl.broadminds.payments.domain;

public enum AccountStatus {
    ACTIVE, SUSPENDED, CLOSED
}
