package pl.broadminds.payments.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Currency;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors
@JsonIdentityInfo(generator= ObjectIdGenerators.IntSequenceGenerator.class, property="@accountOperationId")
public class AccountOperation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private AccountOperationType operationType;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private AccountOperationStatus operationStatus;

    @Basic
    @Column(nullable = false)
    private Instant time;

    @Column(precision = 12, scale = 6, nullable = false)
    private BigDecimal amount;

    @Column(nullable = false, length = 3)
    private Currency currency;

    @Column(precision = 12, scale = 6, nullable = false)
    private BigDecimal otherOperationSideAmount;

    @Column(nullable = false, length = 3)
    private Currency otherOperationSideCurrency;

    @Column(precision = 12, scale = 6, nullable = false)
    private BigDecimal exchangeRate;

    @Column(precision = 12, scale = 6, nullable = false)
    private BigDecimal balance;

    @Column(nullable = false)
    private String description;

//    @OneToOne(optional = false, fetch = FetchType.EAGER)
//    @JoinColumn(name = "ACCOUNT_OPERATION_REQUEST_ID", referencedColumnName = "ID")
//    private AccountOperationRequest accountOperationRequest;

    @ManyToOne
    @JoinColumn(name = "ACCOUNT_ID", nullable = false)
    private Account account;





//    OperationSource
//
//
//
//    @ManyToOne
//    @JoinColumn(nullable = false, name = "DEBITED_ACCOUNT_ID")
//    private Account debitedAccount;
//
//    @ManyToOne
//    @JoinColumn(nullable = false, name = "CREDITED_ACCOUNT_ID")
//    private Account creditedAccount;
}
