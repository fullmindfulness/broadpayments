package pl.broadminds.payments.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import pl.broadminds.payments.domain.validation.ValidationMessages;
import pl.broadminds.payments.security.SpringSecurityAuthority;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors
@JsonIdentityInfo(generator= ObjectIdGenerators.IntSequenceGenerator.class, property="@userId")
@ApiModel(value = "Data of the user.",
        description = "User data along with user's money account and operations.")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ApiModelProperty(position = 0)
    @Column(length = 50, nullable = false, unique = true)
    @Size(min = 4, max = 50)
    private String username;

    @ApiModelProperty(position = 1, hidden = true)
    @Column(length = 100, nullable = false, unique = true)
    @Size(min = 4, max = 100)
    private String password;

    @ApiModelProperty(position = 2)
    @Column(length = 50, nullable = false)
    @Size(min = 2, max = 50)
    private String firstName;

    @ApiModelProperty(position = 3)
    @Column(length = 50, nullable = false)
    @Size(min = 2, max = 50)
    private String lastName;

    @ApiModelProperty(position = 4)
    @Column(length = 11, nullable = false, unique = true)
    @Size(min = 11, max = 11)
    private String pesel;

    @ApiModelProperty(position = 5)
    @Email
    @Column(length = 50, nullable = false)
    private String email;

    @ApiModelProperty(position = 6, hidden = true)
    @Size(max = 50)
    @Column(length = 50, nullable = false, unique = true)
    // TODO Change to UUID https://discourse.hibernate.org/t/persisting-a-custom-uuid-class-as-a-varchar/1404
    private String confirmationCode;

    @ApiModelProperty(position = 7)
    @Column(nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
    private Boolean confirmed;

    @ApiModelProperty(position = 8)
    @Column(nullable = false, columnDefinition = "TINYINT(1) DEFAULT 0")
    private Boolean active;

    @ApiModelProperty(position = 9, hidden = true)
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, columnDefinition = "VARCHAR(10) DEFAULT 'ROLE_USER'")
    private SpringSecurityAuthority role;

    @ApiModelProperty(position = 10)
    @OneToOne(mappedBy = "user", optional = true, fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    private Account account;

    public static UserBuilder defaultValuesUserBuilder() {
        return User.builder()
                .active(Boolean.FALSE)
                .confirmed(Boolean.FALSE)
                .role(SpringSecurityAuthority.ROLE_USER);
    }

    public void confirm() {
        setConfirmed(Boolean.TRUE);
        setActive(Boolean.TRUE);
    }
}
