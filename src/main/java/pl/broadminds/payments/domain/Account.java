package pl.broadminds.payments.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.javamoney.moneta.Money;
import pl.broadminds.cards.domain.Card;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors
@JsonIdentityInfo(generator= ObjectIdGenerators.IntSequenceGenerator.class, property="@accountId")
@ApiModel(value = "Data of the user's money account.",
        description = "Data of user's money account and operations.")
public class Account {

    @Id
    private Long id;

    @OneToOne
    @MapsId
    private User user;

    @ApiModelProperty(position = 1)
    @Column(precision = 12, scale = 6, nullable = false)
    private BigDecimal balance;

    @ApiModelProperty(position = 2)
    @Column(nullable = false, length = 3)
    private Currency currency;

    @ApiModelProperty(position = 3)
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private AccountStatus status;

    @ApiModelProperty(position = 4)
    @OneToOne
    @JoinColumn(name = "CARD_ID", referencedColumnName = "ID")
    private Card card;



//    @OneToMany(mappedBy = "account")
//    private Set<AccountOperationRequest> operationRequests;

    @ApiModelProperty(position = 5)
    @OneToMany(mappedBy = "account")
    private List<AccountOperation> operations;

    public Account debit(Money amount) {
        balance = balance.subtract(amount.getNumber().numberValue(BigDecimal.class));
        return this;
    }

    public Account credit(Money amount) {
        balance = balance.add(amount.getNumber().numberValue(BigDecimal.class));
        return this;
    }

    @JsonIgnore
    public boolean isActive() {
        return AccountStatus.ACTIVE.equals(status);
    }

    public boolean hasEnoughFunds(BigDecimal amount) {
        return balance.compareTo(amount) >= 0;
    }

    public Account close() {
        setBalance(BigDecimal.ZERO);
        setStatus(AccountStatus.CLOSED);
        return this;
    }

    public void activate() {
        setStatus(AccountStatus.ACTIVE);
    }

    public void suspend() {
        setStatus(AccountStatus.SUSPENDED);
    }

    public Account convert(Money money) {
        setBalance(money.getNumber().numberValue(BigDecimal.class));
        // TODO error on purpose - account currency does not change
//        setCurrency(Currency.getInstance(money.getCurrency().getCurrencyCode()));
        setCurrency(currency);
        return this;
    }
}
