package pl.broadminds.payments.domain;

public enum AccountOperationType {
    DEBIT, CREDIT
}
