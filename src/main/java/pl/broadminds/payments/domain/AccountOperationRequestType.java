package pl.broadminds.payments.domain;

public enum AccountOperationRequestType {
    CREDIT_TRANSFER,
    DEBIT_TRANSFER,
    CREDIT_PAYMENT,
    DEBIT_PAYMENT,
    CONVERSION;
}
