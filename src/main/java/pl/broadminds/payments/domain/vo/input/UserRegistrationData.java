package pl.broadminds.payments.domain.vo.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;
import pl.broadminds.payments.domain.User;
import pl.broadminds.payments.domain.validation.ValidationMessages;

import javax.persistence.Column;
import javax.validation.constraints.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@ApiModel(value = "Data of the user for registration.", description = "User data for registration.")
public class UserRegistrationData {

    @ApiModelProperty(name = "username", required = true, position = 0)
    @NotBlank
    @Size(min = 4, max = 20)
    private String username;

    @ApiModelProperty(name = "password", dataType = "String", required = true, position = 1)
    @NotBlank
    @Size(min = 4, max = 20)
    private String password;

    @ApiModelProperty(name = "first name", dataType = "String", required = true, position = 2)
    @NotBlank
    @Size(min = 2, max = 50)
    private String firstName;

    @ApiModelProperty(name = "last name", dataType = "String", required = true, position = 3)
    @NotBlank
    @Size(min = 2, max = 50)
    private String lastName;

    @ApiModelProperty(name = "pesel", dataType = "String", required = true, position = 4)
    @NotBlank
    @Column(unique = true, length = 11)
    @Size(min = 11, max = 11)
    private String pesel;

    @ApiModelProperty(name = "email", dataType = "String", required = true, position = 5)
    @NotBlank
    @Email()
    private String email;
}
