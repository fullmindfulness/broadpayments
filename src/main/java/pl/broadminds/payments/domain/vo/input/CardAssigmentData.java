package pl.broadminds.payments.domain.vo.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.broadminds.payments.domain.validation.ValidationMessages;

import javax.validation.constraints.*;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@ApiModel(value = "Data of the card for assigment to the user's money account.", description = "Card data.")
public class CardAssigmentData {

    @ApiModelProperty(required = true, position = 0)
    @NotNull
    @Size(min = 16, max = 16)
    private String number;

    @ApiModelProperty(required = true, position = 1)
    @NotNull
    @Digits(integer = 3, fraction = 0)
    @Positive
    private Integer csc;

    @ApiModelProperty(required = true, position = 2)
    private LocalDate expirationDate;
}
