package pl.broadminds.payments.domain.vo.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import pl.broadminds.payments.domain.AccountOperationRequestType;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.*;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors
@ApiModel(value = "Transfer data to be executed.", description = "Transfer data.")
public class TransferRequest {

//    @Enumerated(EnumType.STRING)
//    private AccountOperationRequestType type;

    @ApiModelProperty(position = 0, required = true)
    @NotNull
    @DecimalMin("1.00")
    private BigDecimal amount;

    @ApiModelProperty(position = 1, required = true)
    @NotNull
    @Digits(integer = 3, fraction = 0)
    private Integer csc;
}
