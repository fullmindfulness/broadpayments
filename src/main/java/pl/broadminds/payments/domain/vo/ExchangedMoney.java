package pl.broadminds.payments.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.javamoney.moneta.Money;

import javax.money.convert.ExchangeRate;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors
public class ExchangedMoney {

    private Money money;

    private ExchangeRate rate;
}
