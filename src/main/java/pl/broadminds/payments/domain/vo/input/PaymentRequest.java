package pl.broadminds.payments.domain.vo.input;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import pl.broadminds.payments.domain.validation.ValidationMessages;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors
@ApiModel(value = "Payment data to be executed.", description = "Payment data.")
public class PaymentRequest {

    @ApiModelProperty(position = 1)
    @NotBlank(message = ValidationMessages.NOT_BLANK)
    private String creditorUsername;

    @DecimalMin("1.00")
    @NotNull
    private BigDecimal amount;

    @ApiModelProperty(position = 1)
    @NotBlank
    private String description;

}
