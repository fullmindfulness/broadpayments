package pl.broadminds.payments.domain.vo.output;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Currency;

@Data
@Builder
public class PaymentResult {

    private String debtor;
    private String creditor;
    private String description;
    private BigDecimal debitedAmount;
    private Currency debitedCurrency;
    private BigDecimal creditedAmount;
    private Currency creditedCurrency;
    private BigDecimal exchangeRate;
    private BigDecimal commissionValue;
    private BigDecimal commissionAmount;
    private Currency commissionCurrency;
}
