package pl.broadminds.payments.domain;

public enum SystemParameterName {

    COMMISSION, SETTLEMENT_INTERVAL
}
