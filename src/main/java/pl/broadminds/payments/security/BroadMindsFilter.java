package pl.broadminds.payments.security;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BroadMindsFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response, FilterChain filterChain)  throws ServletException, IOException {
        response.addHeader("Jaki kurs jest najlepszy?", "Turbo Kurs w Broad Minds");
        filterChain.doFilter(request, response);
    }
}