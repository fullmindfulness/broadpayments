package pl.broadminds.payments.security;

public enum SpringSecurityAuthority {

    ROLE_USER,
    ROLE_ADMIN
}
